// const matches = require('../data/matches.json')
// const deliveries = require('../data/deliveries.json')

function extraRuns(deliveries, matches){

    let matchId = matches.filter((match) => match.season === "2016").map((match) => match.id)
    // console.log(matchId);

    const runs = deliveries.reduce((acc, cur) => {
        if(matchId.includes(cur.match_id)){
            const extraRuns = cur.extra_runs;
            // console.log(extraRuns);
            const team = cur.bowling_team;
            // console.log(team);
            if(acc[team]){
                acc[team] += parseInt(extraRuns)
            } else {
                acc[team] = parseInt(extraRuns)
            }
        }
        return acc;
    }, {});
    return runs;
}

// extraRuns(deliveries,matches)
module.exports = {extraRuns};