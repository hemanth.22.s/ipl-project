function strikeRate(deliveries, matches){
    const player = deliveries.filter( PlayerData=>PlayerData.batsman === "S Dhawan")
     
    let data = [];
    for(let item of player){
        let years = matches.filter(match => match.id === item.match_id)
        //return years
        for(let i =0;i<years.length; i++){
            item.season = years[i].season
            //console.log(item.season)
            data.push(item);
        }

    }
     let strikeRate = data.reduce((acc,curr)=> {
        if(curr.match_id){
            if(acc[curr.season]){
                acc[curr.season]["runs"]+= parseInt(curr.batsman_runs)
                acc[curr.season]["balls"]++
                acc[curr.season]["strikRate"] = parseFloat((acc[curr.season].runs/acc[curr.season].balls)*100).toFixed(3)
            } else {
                acc[curr.season] = {}
                acc[curr.season]["runs"] = parseInt(curr.batsman_runs)
                acc[curr.season]["balls"] = 1
                }
            }

       return acc;

    },{})


    return {"S Dhawan": Object.fromEntries(Object.entries(strikeRate).map(each => [each[0],each[1].strikRate]))
    }
}

module.exports = {strikeRate}