const matches = require("../data/matches.json")

function matchesPerTeam(matches){
    let wins = matches.reduce((acc, cur) => {
        if(acc[cur.season]){
            if(acc[cur.season][cur.winner]){
                acc[cur.season][cur.winner] += 1;
            } else {
                acc[cur.season][cur.winner] = 1;
            }
        } else {
            acc[cur.season] = {}
            if(acc[cur.season][cur.winner]){
                acc[cur.season][cur.winner] += 1;
            } else {
                acc[cur.season][cur.winner] = 1;
            }
        }
        return acc;
    },{})

    return wins;
}

module.exports = {matchesPerTeam}

// matchesPerTeam(matches);
