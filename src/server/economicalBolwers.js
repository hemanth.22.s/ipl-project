function economicalBowlers(matches,deliveries) {
    let bowlerData = {}
    for(match of matches){
        if (match.season == 2015){
            // console.log(match, "hii")
            for (let delivery of deliveries) {
                // console.log(delivery);
                if (match.id == delivery.match_id) {
                    // console.log(match.id)
                    if (bowlerData[delivery.bowler]) {
                        bowlerData[delivery.bowler].balls += 1
                        bowlerData[delivery.bowler].runs += parseInt(delivery.total_runs)
                        // console.log(BowlerData);
                    } else {
                        let temp = {}
                        temp.balls = 1
                        temp.runs = parseInt(delivery.total_runs)
                        bowlerData[delivery.bowler] = temp
                    } 
                } 
            }
        } 
    }
    
    //return BowlerData
    let arrayOfEachPlayerEconamy = []
   // let eachPlayerEconomy = {}
    for (let item in bowlerData) {
        let eachPlayerEconomy = {}
        let numberOfOvers = (bowlerData[item].balls / 6)
        let economy = (bowlerData[item].runs / numberOfOvers) 
        eachPlayerEconomy[item] = economy
        arrayOfEachPlayerEconamy.push(eachPlayerEconomy)
    }
//return arrayOfEachPlayerEconamy
  
let sortedArray = arrayOfEachPlayerEconamy.sort((a, b) => a[Object.keys(a)] - b[Object.keys(b)])
//return sortedArray

let topTen=sortedArray.splice(0,10)
return topTen

}
// console.log(economicalBowlers(matches, deliveries));
module.exports= {economicalBowlers}