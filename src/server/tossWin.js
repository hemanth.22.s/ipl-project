// const matches = require("../data/matches.json")

function tossWin (matches){
  const toss = matches.reduce((acc, cur) => {
    if (acc[cur.toss_winner]) {
      if (cur.toss_winner === cur.winner) {
        acc[cur.toss_winner] += 1;
      }
    } else {
      if (cur.toss_winner === cur.winner) {
        acc[cur.toss_winner] = 1;
      }
    }
    return acc;
  }, {});

  return toss;
};

// console.log(tossWin(matches));
module.exports = {tossWin};
