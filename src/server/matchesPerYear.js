function matchesPerYear(matches){
    let result = matches.reduce((acc, cur) => {
        if(acc[cur.season]){
            // console.log(acc);
                acc[cur.season] +=1
            //  console.log(acc);
            } else {
                acc[cur.season] = 1
        }
        return acc;
    },{})
    return result;
}

module.exports = {matchesPerYear}

// matchesPerYear(matches);
