const fs = require('fs')
const csv = require('csvtojson');

const csvFilePath1 = './data/matches.csv';
const csvFilePath2 = './data/deliveries.csv';

const jsonFilePath1 = './data/matches.json';
const jsonFilePath2 = './data/deliveries.json';


function csvToJson(filePath,fileName){
    csv()
    .fromFile(filePath)
    .then((json) => {

        fs.writeFile(fileName,JSON.stringify(json, null, 2),'utf-8',(err)=>{
            if(err) console.log(err)
        })
    })
}

csvToJson(csvFilePath1,jsonFilePath1);
csvToJson(csvFilePath2,jsonFilePath2);
