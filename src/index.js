const matches = require ("../src/data/matches.json");
const deliveries = require("../src/data/deliveries.json");
const fs = require ("fs");

function outputJson(filePath, fileName){
    fs.writeFile(filePath, JSON.stringify(fileName,null,2), (err) =>{
        if (err){
            console.log(err);
        }
    })
}

const iplYear = require("./server/matchesPerYear");
const matchesPlayedPerYear = iplYear.matchesPerYear(matches);
const filepath1 = "./public/output/matchesPlayedperYear.json"
outputJson(filepath1, matchesPlayedPerYear) 

const iplWon = require("./server/matchesPerTeam");
const matchesWonPerTeam = iplWon.matchesPerTeam(matches);
const filepath2 = "./public/output/matchesWonPerTeam.json"
outputJson(filepath2, matchesWonPerTeam)

const iplRuns = require("./server/extraRuns")
const extraRunsPerTeam = iplRuns.extraRuns(deliveries, matches);
const filepath3 = "./public/output/extraRunsPerTeam.json"
outputJson(filepath3, extraRunsPerTeam)

const iplTop = require("./server/economicalBolwers")
const topEconomicalBowlers = iplTop.economicalBowlers(matches, deliveries)
const filePath4 = './public/output/topEconomicalBowlers2015.json'
outputJson(filePath4,topEconomicalBowlers)

const iplToss = require("./server/tossWin")
const matchTossWinner = iplToss.tossWin(matches);
const filepath5 = "./public/output/matchTossWinner.json"
outputJson(filepath5, matchTossWinner)

const iplAward = require("./server/awardMatch")
const awardWinner = iplAward.awardMatch(matches);
const filepath6 = "./public/output/awardWinner.json"
outputJson(filepath6, awardWinner)

const iplDismiss = require("./server/highestdismiss")
const highestDismissal= iplDismiss.highestDismiss(deliveries);
const filepath7 = "./public/output/highestDismissal.json"
outputJson(filepath7, highestDismissal)

const iplOver = require('./server/economySuperOver')
const economicOver = iplOver.economySuperOver(deliveries);
const filepath8 = "./public/output/topEconomicSuperOver.json"
outputJson(filepath8, economicOver)

const iplStrike = require('./server/strikeRate')
const strikeRateSeason = iplStrike.strikeRate(deliveries, matches);
const filepath9 = "./public/output/strikeRatePerSeason.json"
outputJson(filepath9, strikeRateSeason)